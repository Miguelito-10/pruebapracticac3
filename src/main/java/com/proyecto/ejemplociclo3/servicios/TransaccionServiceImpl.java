/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.proyecto.ejemplociclo3.servicios;

import com.proyecto.ejemplociclo3.interfaces.TransaccionService;
import com.proyecto.ejemplociclo3.modelo.Transaccion;
import com.proyecto.ejemplociclo3.servicios.repositorios.TransaccionDAO;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author CARLOS
 */
@Service
public class TransaccionServiceImpl implements TransaccionService{
     
     @Autowired
     private TransaccionDAO transaccionDao;

     @Override
     @Transactional(readOnly = false)
     public Transaccion save(Transaccion transaccion){
          return transaccionDao.save(transaccion);
     }

     @Override
     @Transactional(readOnly = false)
     public void delete (Integer id){
          transaccionDao.deleteById(id);
     }

     @Override
     @Transactional(readOnly = true)
     public Transaccion findById(Integer id){
     return transaccionDao.findById(id).orElse(null);
     }

     @Override
     @Transactional(readOnly = true)
     public List<Transaccion> findAll(){
     return (List<Transaccion>) transaccionDao.findAll();
     }

}
