/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.proyecto.ejemplociclo3.interfaces;

import com.proyecto.ejemplociclo3.modelo.Detalle;
import java.util.List;

/**
 *
 * @author CARLOS
 */
public interface DetalleService {
   
      public Detalle save(Detalle detalle);
      public void delete(Integer id);
      public Detalle findById(Integer id);
      public List<Detalle> findAll();  
}
