/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.proyecto.ejemplociclo3.interfaces;

import com.proyecto.ejemplociclo3.modelo.Transaccion;
import java.util.List;

/**
 *
 * @author CARLOS
 */
public interface TransaccionService {
      public Transaccion save(Transaccion transaccion);
      public void delete(Integer id);
      public Transaccion findById(Integer id);
      public List<Transaccion> findAll();

}
