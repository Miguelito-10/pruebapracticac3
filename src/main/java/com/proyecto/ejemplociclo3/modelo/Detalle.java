/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.proyecto.ejemplociclo3.modelo;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author CARLOS
 */
@Entity
@Table(name="detalle")
public class Detalle implements Serializable{
     @Id
     @GeneratedValue(strategy = GenerationType.IDENTITY)
     @Column(name="idDetalle")
     private Integer idDetalle;

     @ManyToOne
     @JoinColumn(name="idProducto")
     private Producto producto;

     @ManyToOne
     @JoinColumn(name="idTransaccion")
     private Transaccion transaccion;

     @Column(name="valorDetalle")
     private double valorDetalle;

     @Column(name="cantidadDetalle")
     private double cantidadDetalle;

     @Column(name="totalDetalle")
     private double totalDetalle;

     public Detalle(Integer idDetalle, Producto producto, Transaccion transaccion, double valorDetalle, double cantidadDetalle, double totalDetalle) {
          this.idDetalle = idDetalle;
          this.producto = producto;
          this.transaccion = transaccion;
          this.valorDetalle = valorDetalle;
          this.cantidadDetalle = cantidadDetalle;
          this.totalDetalle = totalDetalle;
     }

     public Detalle() {
     }



     public Integer getIdDetalle() {
          return idDetalle;
     }

     public void setIdDetalle(Integer idDetalle) {
          this.idDetalle = idDetalle;
     }

     public Producto getProducto() {
          return producto;
     }

     public void setProducto(Producto producto) {
          this.producto = producto;
     }

     public Transaccion getTransaccion() {
          return transaccion;
     }

     public void setTransaccion(Transaccion transaccion) {
          this.transaccion = transaccion;
     }

     public double getValorDetalle() {
          return valorDetalle;
     }

     public void setValorDetalle(double valorDetalle) {
          this.valorDetalle = valorDetalle;
     }

     public double getCantidadDetalle() {
          return cantidadDetalle;
     }

     public void setCantidadDetalle(double cantidadDetalle) {
          this.cantidadDetalle = cantidadDetalle;
     }

     public double getTotalDetalle() {
          return totalDetalle;
     }

     public void setTotalDetalle(double totalDetalle) {
          this.totalDetalle = totalDetalle;
     }


}
